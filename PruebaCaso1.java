class Empleado {
    
    private String primerNombre;
    private String apellidoPaterno;
    private String numeroSerguroSocial;
    
// constructor con 3 argumentos
    public Empleado (String primerNombre, String apellidoPaterno, String numeroSerguroSocial ){
        
        this.primerNombre = primerNombre;
        this.apellidoPaterno = apellidoPaterno;
        this.numeroSerguroSocial = numeroSerguroSocial;
    } // fin del constructor  
    // devuelve el primer nombre
    public String obtenerPrimerNombre(){
        return this.primerNombre;
    }
    // devuelve el apellido paterno
    public String obtenerApellidoPaterno(){
        return this.apellidoPaterno;
    }
    // devuelve el numero de seguro sacial
    public String obtenerNumeroSeguroSocial(){
        return this.numeroSerguroSocial;
    }
    
    public String toString(){
        return String.format("%s: %s %s%n%s: %s", "Empleado", obtenerPrimerNombre(), 
               obtenerApellidoPaterno(), "Numero de seguro social", obtenerNumeroSeguroSocial());
    }

}
class EmpleadoPorHoras extends Empleado{
    
    private double horas;
    private double sueldo;
    
    // constructor con 3 argumentos
    public EmpleadoPorHoras(String primerNombre, String apellidoPaterno, String numeroSerguroSocial, double horas, double sueldo) {
         // llamada explícita al constructor de la superclase Empleado
        super(primerNombre, apellidoPaterno, numeroSerguroSocial);
        
        // si sueldo Base no es válido, lanza excepción
        if ( sueldo <= 0.0){
            throw new IllegalArgumentException("El salario por hora debe ser > 0.0");
        }
        // si las horas no es válido, lanza excepción
        if (horas < 0.0 || horas > 168.0 ){
            throw new IllegalArgumentException("El numero de hora trabajadas debe ser >= 0.0 y < 168.0");
        }
        this.sueldo = sueldo;
        this.horas = horas;
    }
    
    public void establecerHoras (double horas){
    
        if (horas < 0.0 || horas > 168.0){
            throw new IllegalArgumentException("El numero de hora trabajadas debe ser >= 0.0 y < 168.0");
        }
        this.horas = horas;
    }
    
    public double obtenerHoras(){
    
        return this.horas;
    }
    
    public void establecerSueldo (double sueldo){
    
        if (sueldo <= 0.0){
        
            throw new IllegalArgumentException("El salario por hora debe ser > 0.0");
        }
        this.sueldo = sueldo;
    }
    
    public double obtenerSueldo(){
        
        return this.sueldo; 
    }
    
    public double ganancias(){
        
        double e = obtenerHoras() * obtenerSueldo();
        if (obtenerHoras() > 40.0){
            
            e += (obtenerHoras() - 40) * 1.5 * obtenerSueldo();
        }
        return e;
    }
 
    @Override
    public String toString(){
        
        return String.format("%s %s%n%s: %.2f%n%s: %.2f","Por/Horas", super.toString(),"Horas de Trabajo", 
                             obtenerSueldo(), "Numero de horas Trabajadas", obtenerHoras()); 
    }
}
public class PruebaCaso1 {
    
    public static void main(String[] args){
    
        EmpleadoPorHoras E = new EmpleadoPorHoras("Shuvert","Jaya","123-45-6789", 10.0, 120.0);
        System.out.println("Información del empleado");
        System.out.printf("Nombre: %s%n", E.obtenerPrimerNombre());
        System.out.printf("Apellido: %s%n", E.obtenerApellidoPaterno());
        System.out.printf("Numero de seguro social: %s%n", E.obtenerNumeroSeguroSocial());
        System.out.printf("Sueldo: %s%n", E.obtenerSueldo());
        System.out.printf("Horas: %s%n", E.obtenerHoras());
        
        E.establecerSueldo(15.0);
        E.establecerHoras(125.0);
        
        System.out.printf("%nInformación actualizada del empleado%n%n%s", E);
    }
  
}